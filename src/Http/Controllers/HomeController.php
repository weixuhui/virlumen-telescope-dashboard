<?php

namespace Virchow\VirlumenTelescopeDashboard\Http\Controllers;

use Illuminate\Routing\Controller;
use \Virchow\VirlumenTelescopeDashboard\Telescope;

class HomeController extends Controller
{
    /**
     * Display the Telescope view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectId = request('projectId',0);
        $projectInfo = \App\Models\Project::query()->find($projectId);

        return view('telescope::layout', [
            'cssFile' => Telescope::$useDarkTheme ? 'app-dark.css' : 'app.css',
            'telescopeScriptVariables' => Telescope::scriptVariables(),
            'projectInfo' => $projectInfo
        ]);
    }
}
