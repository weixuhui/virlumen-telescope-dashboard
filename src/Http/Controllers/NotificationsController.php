<?php

namespace Virchow\VirlumenTelescopeDashboard\Http\Controllers;

use Virchow\VirlumenTelescopeDashboard\EntryType;
use Virchow\VirlumenTelescopeDashboard\Watchers\NotificationWatcher;

class NotificationsController extends EntryController
{
    /**
     * The entry type for the controller.
     *
     * @return string
     */
    protected function entryType()
    {
        return EntryType::NOTIFICATION;
    }

    /**
     * The watcher class for the controller.
     *
     * @return string
     */
    protected function watcher()
    {
        return NotificationWatcher::class;
    }
}
