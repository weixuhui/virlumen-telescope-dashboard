<?php

namespace Virchow\VirlumenTelescopeDashboard\Http\Controllers;

use Virchow\VirlumenTelescopeDashboard\EntryType;
use Virchow\VirlumenTelescopeDashboard\Watchers\ViewWatcher;

class ViewsController extends EntryController
{
    /**
     * The entry type for the controller.
     *
     * @return string
     */
    protected function entryType()
    {
        return EntryType::VIEW;
    }

    /**
     * The watcher class for the controller.
     *
     * @return string
     */
    protected function watcher()
    {
        return ViewWatcher::class;
    }
}
