<?php

namespace Virchow\VirlumenTelescopeDashboard\Http\Controllers;

use Virchow\VirlumenTelescopeDashboard\EntryType;
use Virchow\VirlumenTelescopeDashboard\Watchers\MailWatcher;

class MailController extends EntryController
{
    /**
     * The entry type for the controller.
     *
     * @return string
     */
    protected function entryType()
    {
        return EntryType::MAIL;
    }

    /**
     * The watcher class for the controller.
     *
     * @return string
     */
    protected function watcher()
    {
        return MailWatcher::class;
    }
}
