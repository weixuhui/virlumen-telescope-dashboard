<?php

namespace Virchow\VirlumenTelescopeDashboard\Http\Controllers;

use Illuminate\Routing\Controller;
use Virchow\VirlumenTelescopeDashboard\Contracts\EntriesRepository;

class MailHtmlController extends Controller
{
    /**
     * Get the HTML content of the given email.
     *
     * @param  \Laravel\Telescope\Contracts\EntriesRepository  $storage
     * @param  int  $id
     * @return mixed
     */
    public function show(EntriesRepository $storage, $id)
    {
        return $storage->find($id)->content['html'];
    }
}
