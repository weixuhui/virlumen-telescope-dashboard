<?php

namespace Virchow\VirlumenTelescopeDashboard\Http\Controllers;

use Illuminate\Routing\Controller;
use Virchow\VirlumenTelescopeDashboard\Contracts\EntriesRepository;

class MailEmlController extends Controller
{
    /**
     * Download the Eml content of the email.
     *
     * @param  \Laravel\Telescope\Contracts\EntriesRepository  $storage
     * @param  int  $id
     * @return mixed
     */
    public function show(EntriesRepository $storage, $id)
    {
        return response($storage->find($id)->content['raw'], 200, [
            'Content-Type' => 'message/rfc822',
            'Content-Disposition' => 'attachment; filename="mail-'.$id.'.eml"',
        ]);
    }
}
