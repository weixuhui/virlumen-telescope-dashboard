<?php

namespace Virchow\VirlumenTelescopeDashboard\Http\Controllers;

use Virchow\VirlumenTelescopeDashboard\EntryType;
use Virchow\VirlumenTelescopeDashboard\Watchers\GateWatcher;

class GatesController extends EntryController
{
    /**
     * The entry type for the controller.
     *
     * @return string
     */
    protected function entryType()
    {
        return EntryType::GATE;
    }

    /**
     * The watcher class for the controller.
     *
     * @return string
     */
    protected function watcher()
    {
        return GateWatcher::class;
    }
}
